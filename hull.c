//Poirson Sean
//Rothé Antonin

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 1024

//./hull-generator 100 | ./hull-viewer ./hull
//gcc -Wall -std=c99 -O2 -g -o hull hull.c




//Création du type vec
//Composé de 2 doubles x et y
//x, l'abscisse du point P
//y, l'ordonnée du point P
struct vec {
    double x;
    double y;
};

//Retourne le produit scalaire de v1 et v2
//Le produit scalaire représente la coordonnée du projeté de V1 sur un axe de vecteur unité V2.
//Entrée : vecteur v1 et vecteur v2
double dot(const struct vec *v1, const struct vec *v2){
    return ((v1->x*v2->x)+(v1->y*v2->y));
    
};

//Retourne le produit vectoriel 2D^2 de deux vecteurs P1P2 et P1P3
//Le produit vectoriel 2D détermine si P3 est à gauche ou à doite de la droite (P1P2)
//Entrée : 3 vecteurs : p1,p2 et p3
double cross(const struct vec *p1, const struct vec *p2, const struct vec *p3){
    return ((((p2->x)-(p1->x))*((p3->y)-(p1->y)))-(((p2->y)-(p1->y))*((p3->x)-(p1->x))));
};



//Retourne True si la suite p1,p2,P3 représente un tournant à gauche, false sinon
//Entrée : 3 vecteurs : p1,p2 et p3
bool is_left_turn(const struct vec *p1,const struct vec *p2, const struct vec *p3){
    return false;
    
};

//tableau dynamique de points
//représente un ensemble de points
struct vecset {
struct vec *data;
size_t size;
size_t capacity;
};

//Crée un ensemble de points vide
void vecset_create(struct vecset *self){
    self->capacity=10;
    self->data=malloc(self->capacity*sizeof(int));
    self->size=0;
    
}

//Détruit un ensemble de points
void vecset_destroy(struct vecset *self){
    free(self->data);
    self->data =NULL;
    self->capacity=0;
    self->size=0;
}

//Ajoute un point à un ensemble de points
void vecset_add(struct vecset *self, struct vec p){
    if(self->size==self->capacity){
        self->capacity++;
    }
    self->data[self->size]=p;
    self->size++;
    
}

/*On considère une fonction de comparaison de points avec un contexte qui
renvoie un entier strictement négatif si p1 est «plus petit» que p2, un entier
strictement positif si p1 est «plus grand» que p2 et 0 si p1 est «égal» à p2.*/
typedef int (*comp_func_t)(const struct vec *p1,const struct vec *p2, const void *ctx);

//Renvoie le maximum d'un ensemble de points suivant une fonction de comparaison donnée
const struct vec *vecset_max(const struct vecset *self,comp_func_t func, const void *ctx){
    struct vec *max = &self->data[0];
    for(size_t i=0;i<self->size;i++){
        if((func(&self->data[i],max,ctx))>0){
            max=&self->data[i];
        }
    }
    return max;
}

//Renvoie le minimum d'un ensemble de points suivant une fonction de comparaison donnée
const struct vec *vecset_min(const struct vecset *self,comp_func_t func, const void *ctx){
    struct vec *min = &self->data[0];
    for(size_t i=0;i<self->size;i++){
        if((func(&self->data[i],min,ctx))<0){
            min=&self->data[i];
        }
    }
    return min;
}

//Trie l'ensemble des points selon la fonction de comparaison donnée
void vecset_sort(struct vecset *self, comp_func_t func,const void *ctx){
    /*struct vecset *tab=NULL;
    vecset_create(tab);
    for(size_t i=0;i<self->size;i++){
        if((func(&self->data[i],&self->data[i+1],ctx))<0){
            tab->data[i]=&self->data[i];
        }else{
            tab->data[i]=&self->data[i+1];
        }
    }*/
}

//empile un élément
void vecset_push(struct vecset *self, struct vec p){
    if(self->size==self->capacity){
        self->capacity++;
    }
    struct vecset *tab=NULL;
    vecset_create(tab);
    memcpy(tab,self->data,self->size*sizeof(struct vec));
    free(self->data);
    self->data[0]=p;
    self->size++;
    for(size_t i=1;i<=self->size;i++){
        self->data[i]=tab->data[i-1];
    }
}

//Dépile un élément
void vecset_pop(struct vecset *self){
    //self->data[0]=NULL;
    for(size_t i =1;i<=self->size;i++){
        self->data[i-1]=self->data[i];
    }
    
}

//renvoi le premier élément de la pile
const struct vec *vecset_top(const struct vecset *self){
    struct vec *premier;
    premier=&self->data[0];
    return premier;
}

//renvoi le second élément de la pile
const struct vec *vecset_second(const struct vecset *self){
    struct vec *second;
    second=&self->data[1];
    return second;
}


//MARCHE DE JARVIS
void jarvis_march(const struct vecset *in, struct vecset *out){
    
}
int main() {
    setbuf(stdout, NULL); // avoid buffering in the output
    char buffer[BUFSIZE];
    fgets(buffer, BUFSIZE, stdin);
    size_t count = strtol(buffer, NULL, 10);
    for (size_t i = 0; i < count; ++i) {
        struct vec p;
        fgets(buffer, BUFSIZE, stdin);
        char *endptr = buffer;
        p.x = strtod(endptr, &endptr);
        p.y = strtod(endptr, &endptr);
        // then do something with p
    }
    return 0;
}


